import matplotlib.pyplot as plt
import numpy as np
import sys

#Wczytanie danych z pliku (ignoruj dwie pierwsze linie)
with open('sin', 'r') as file:
    lines = file.readlines()[2:]
    data = [line.strip().split(',') for line in lines]
    variable1 = [float(entry[0]) for entry in data]
    variable2 = [float(entry[1]) for entry in data]
    variable3 = [float(entry[2]) for entry in data]
    variable4 = [float(entry[3]) for entry in data]
    variable5 = [float(entry[4]) for entry in data]
    variable6 = [float(entry[5]) for entry in data]

# with open('turb', 'r') as file:
#     lines = file.readlines()[2:]
#     data = [line.strip().split(',') for line in lines]
#     variable1s = [float(entry[0]) for entry in data]
#     variable2s = [float(entry[1]) for entry in data]
#     variable3s = [float(entry[2]) for entry in data]
#     variable4s = [float(entry[3]) for entry in data]
#     variable5s = [float(entry[4]) for entry in data]
#     variable6s = [float(entry[5]) for entry in data]

# with open('turb_noS', 'r') as file:
#     lines = file.readlines()[2:]
#     data = [line.strip().split(',') for line in lines]
#     variable1 = [float(entry[0]) for entry in data]
#     variable2 = [float(entry[1]) for entry in data]
#     variable3 = [float(entry[2]) for entry in data]
#     variable4 = [float(entry[3]) for entry in data]
#     variable5 = [float(entry[4]) for entry in data]
#     variable6 = [float(entry[5]) for entry in data]

# with open('zero', 'r') as file:
#     lines = file.readlines()[2:]
#     data = [line.strip().split(',') for line in lines]
#     variable0 = [float(entry[3]) for entry in data]
#     variable1 = [float(entry[0]) for entry in data]
# with open('piec', 'r') as file:
#     lines = file.readlines()[2:]
#     data = [line.strip().split(',') for line in lines]
#     variable5 = [float(entry[3]) for entry in data]
# with open('dziesiec', 'r') as file:
#     lines = file.readlines()[2:]
#     data = [line.strip().split(',') for line in lines]
#     variable10 = [float(entry[3]) for entry in data]
# with open('pietnascie', 'r') as file:
#     lines = file.readlines()[2:]
#     data = [line.strip().split(',') for line in lines]
#     variable15 = [float(entry[3]) for entry in data]
# with open('dwadziesciapiec', 'r') as file:
#     lines = file.readlines()[2:]
#     data = [line.strip().split(',') for line in lines]
#     variable25 = [float(entry[3]) for entry in data]
# with open('trzydziescipiec', 'r') as file:
#     lines = file.readlines()[2:]
#     data = [line.strip().split(',') for line in lines]
#     variable35 = [float(entry[3]) for entry in data]

# Step czasowy i zakres czasu
time_step = 0.002
start_time = 0
end_time = len(variable1) * time_step

# Tworzenie osi czasu
time_axis = np.arange(start_time, end_time, time_step)

# # Wykres dla Zmiennej 1
# plt.figure(figsize=(6, 4))
# plt.plot(time_axis, variable1, color='blue')
# plt.title('setpoint')
# plt.xlabel('Czas[s]')
# plt.ylabel('setpoint')
# plt.grid(True)
# plt.savefig(sys.argv[1]+'czas.png', bbox_inches='tight')

# # Wykres dla Zmiennej 2
# plt.figure(figsize=(6, 4))
# plt.plot(time_axis, variable2, color='blue')
# plt.title('Wysokość lotu')
# plt.xlabel('Czas[s]')
# plt.ylabel('Wysokość [m]')
# plt.grid(True)
# plt.savefig(sys.argv[1]+'wysokosc.png', bbox_inches='tight')

# # Wykres dla Zmiennej 3
# plt.figure(figsize=(6, 4))
# plt.plot(time_axis, variable3, color='blue')
# plt.title('Prędkość rakiety')
# plt.xlabel('Czas[s]')
# plt.ylabel('Prędkość [$\\frac{m}{s}$]')
# plt.grid(True)
# plt.savefig(sys.argv[1]+'predkosc.png', bbox_inches='tight')

# # Wykres dla Zmiennej 4
# plt.figure(figsize=(6, 4))
# plt.plot(time_axis, variable4, color='blue')
# plt.title('Prędkość obrotowa rakiety')
# plt.xlabel('Czas[s]')
# plt.ylabel('Prędkość obrotowa [$\\frac{°}{s}$]')
# plt.grid(True)
# plt.savefig(sys.argv[1]+'pobrotowa.png', bbox_inches='tight')

# # Wykres dla Zmiennej 5
# plt.figure(figsize=(6, 4))
# plt.plot(time_axis, variable5, color='blue')
# plt.title('Turbulencje')
# plt.xlabel('Czas[s]')
# plt.ylabel('Turbulencje [$\\frac{m}{s}$]')
# plt.grid(True)
# plt.savefig(sys.argv[1]+'turbulencje.png', bbox_inches='tight')

# # Wykres dla Zmiennej 6
# plt.figure(figsize=(6, 4))
# plt.plot(time_axis, variable6, color='blue')
# plt.title('Wychylenie lotek rakiety')
# plt.xlabel('Czas[s]')
# plt.ylabel('Wychylenie lotek [°]')
# plt.grid(True)
# plt.savefig(sys.argv[1]+'lotki.png', bbox_inches='tight')

# Wykres dla porównania
# plt.figure(figsize=(9 , 6))
# plt.plot(time_axis[0:500], variable0[0:500], color='red', label='ω = 0$\\frac{rad}{s}$')
# plt.plot(time_axis[0:500], variable5[0:500], linestyle='--', color='green', label='ω = 5$\\frac{rad}{s}$')
# plt.plot(time_axis[0:500], variable10[0:500], linestyle='-.', color='blue', label='ω = 10$\\frac{rad}{s}$')
# plt.plot(time_axis[0:500], variable15[0:500], linestyle=':', color='magenta', label='ω = 15$\\frac{rad}{s}$')
# plt.plot(time_axis[0:500], variable25[0:500], linestyle='-', color='turquoise', label='ω = 25$\\frac{rad}{s}$')
# plt.plot(time_axis[0:500], variable35[0:500], linestyle='--', color='orange', label='ω = 35$\\frac{rad}{s}$')
# plt.xlabel('Czas[s]')
# plt.ylabel('Prędkość obrotowa [$\\frac{rad}{s}$]')
# plt.grid(True)
# plt.legend()
# plt.savefig('../../misc/diff_omega.png', bbox_inches='tight')

# Comp for turb
# plt.figure(figsize=(9 , 6))
# plt.plot(time_axis, variable4s, color='blue', linestyle='-', label = 'Prędkość obrotowa ze stabilizacją')
# plt.plot(time_axis, variable4, color='red', linestyle='--',  label = 'Prędkość obrotowa bez stabilizacją')
# plt.xlabel('Czas[s]')
# plt.ylabel('Prędkość obrotowa [$\\frac{rad}{s}$]')
# plt.grid(True)
# plt.legend()
# plt.savefig('../../misc/turb.png', bbox_inches='tight')

# Comp for sinus
# plt.figure(figsize=(9 , 6))
# plt.plot(time_axis, variable1, color='blue', linestyle='-', label = 'Prędkość obrotowa ze stabilizacją')
# plt.plot(time_axis, variable4, color='red', linestyle='--',  label = 'Prędkość obrotowa bez stabilizacją')
# plt.xlabel('Czas[s]')
# plt.ylabel('Prędkość obrotowa [$\\frac{rad}{s}$]')
# plt.grid(True)
# plt.legend()
# plt.savefig('../../misc/sin.png', bbox_inches='tight')

roznice = [x - y for x, y in zip(variable1, variable4)]
plt.figure(figsize=(9 , 6))
plt.plot(time_axis, roznice, color='blue', linestyle='-')
plt.xlabel('Czas[s]')
plt.ylabel('Błąd śledzenia trajektori [$\\frac{rad}{s}$]')
plt.grid(True)
plt.savefig('../../misc/sinEr.png', bbox_inches='tight')