#!/bin/bash
cd output

for file in *; do
    if [ -f "$file" ]; then
        echo "Found: $file"
        mkdir "A$file"
        cd "A$file"
        python3 ../../vis.py ../$file
        cd ..
    else
        echo "Cannot find $file"
    fi
done

