import matplotlib.pyplot as plt
import numpy as np
import sys

# Wczytanie danych z pliku (ignoruj dwie pierwsze linie)
with open(sys.argv[1], 'r') as file:
    lines = file.readlines()[2:]
    data = [line.strip().split(',') for line in lines]
    variable1 = [float(entry[0]) for entry in data]
    variable2 = [float(entry[1]) for entry in data]
    variable3 = [float(entry[2]) for entry in data]
    variable4 = [float(entry[3]) for entry in data]
    variable5 = [float(entry[4]) for entry in data]
    variable6 = [float(entry[5]) for entry in data]

# Step czasowy i zakres czasu
time_step = 0.002
start_time = 0
end_time = len(variable1) * time_step

# Tworzenie osi czasu
time_axis = np.arange(start_time, end_time, time_step)

# Wykres dla Zmiennej 1
plt.figure(figsize=(6, 4))
plt.plot(time_axis, variable1, color='blue')
plt.title('Czas')
plt.xlabel('Czas[s]')
plt.ylabel('Czas[s]')
plt.grid(True)
plt.savefig(sys.argv[1]+'czas.png', bbox_inches='tight')

# Wykres dla Zmiennej 2
plt.figure(figsize=(6, 4))
plt.plot(time_axis, variable2, color='blue')
plt.title('Wysokość lotu')
plt.xlabel('Czas[s]')
plt.ylabel('Wysokość [m]')
plt.grid(True)
plt.savefig(sys.argv[1]+'wysokosc.png', bbox_inches='tight')

# Wykres dla Zmiennej 3
plt.figure(figsize=(6, 4))
plt.plot(time_axis, variable3, color='blue')
plt.title('Prędkość rakiety')
plt.xlabel('Czas[s]')
plt.ylabel('Prędkość [$\\frac{m}{s}$]')
plt.grid(True)
plt.savefig(sys.argv[1]+'predkosc.png', bbox_inches='tight')

# Wykres dla Zmiennej 4
plt.figure(figsize=(6, 4))
plt.plot(time_axis, variable4, color='blue')
plt.title('Prędkość obrotowa rakiety')
plt.xlabel('Czas[s]')
plt.ylabel('Prędkość obrotowa [$\\frac{°}{s}$]')
plt.grid(True)
plt.savefig(sys.argv[1]+'pobrotowa.png', bbox_inches='tight')

# Wykres dla Zmiennej 5
plt.figure(figsize=(6, 4))
plt.plot(time_axis, variable5, color='blue')
plt.title('Turbulencje')
plt.xlabel('Czas[s]')
plt.ylabel('Turbulencje [$\\frac{m}{s}$]')
plt.grid(True)
plt.savefig(sys.argv[1]+'turbulencje.png', bbox_inches='tight')

# Wykres dla Zmiennej 6
plt.figure(figsize=(6, 4))
plt.plot(time_axis, variable6, color='blue')
plt.title('Wychylenie lotek rakiety')
plt.xlabel('Czas[s]')
plt.ylabel('Wychylenie lotek [°]')
plt.grid(True)
plt.savefig(sys.argv[1]+'lotki.png', bbox_inches='tight')

